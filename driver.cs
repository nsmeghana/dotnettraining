﻿using System;
using Inheritance;
using Encapsulation;
using Polymorphism;

namespace OOP
{
    class oopConcepts
    {
        static void Main(string[] args)
        {
            //Encapsulation
            Encapsulation.Interest si = new Encapsulation.Interest();
            si.getData(12000, 4, 7);
            Console.WriteLine("Simple interest is : " + si.getSimpleInterest());

            //Inheritance 
            Rectangle r = new Rectangle();
            Triangle t = new Triangle();
            r.length = 34;
            r.breadth = 7;
            Console.WriteLine("Area of the rectangle is: " + r.getArea());

            //Polymorphism
            SimpleCalculator cal1 = new SimpleCalculator();
            cal1.add(6, -9);
            cal1.add(3.7F, 11.6F);

            Polymorphism.simpleInterest sInt = new Polymorphism.simpleInterest(11000, 6, 4);
            Console.WriteLine("Simple Interest is: " + sInt.getInterest());
            Polymorphism.compoundInterest cInt = new Polymorphism.compoundInterest(13000, 5, 7);
            Console.WriteLine("Compound Interest is: " + cInt.getInterest());
        }
    }
}
