﻿using System;

namespace Encapsulation
{
    class Interest
    {
        private double principal, time, rate;

        public void getData(double principal, double time, double rate)
        {
            this.principal = principal;
            this.time = time;
            this.rate = rate;
        }

        public double getSimpleInterest()
        {
            return (principal * time * rate) / 100;
        }
    }
}