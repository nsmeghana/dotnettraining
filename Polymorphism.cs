﻿using System;

namespace Polymorphism
{
    class SimpleCalculator
        {
            public void add(int iVar1, int iVar2)
            {
                Console.WriteLine("Sum is " + (iVar1 + iVar2));
            }

        public void subtract(int iVar1, int iVar2)
            {
                Console.WriteLine("Difference is " + (iVar1 - iVar2));
            }

        public void multiply(int iVar1, int iVar2)
            {
               Console.WriteLine("Product is: " + (iVar1 * iVar2));
            }

        public void divide(int iVar1, int iVar2)
            {
                try
                {
                    Console.WriteLine("Quotient is: " + (iVar1 / iVar2));
                }

                catch
                {
                    Console.WriteLine("Invalid input");
                }
            }

        public void add(float fVar1, float fVar2)
            {
               Console.WriteLine("Sum is: " + (fVar1 + fVar2));
            }

        public void subtract(float fVar1, float fVar2)
            {
                Console.WriteLine("Difference is: " + (fVar1 - fVar2));
            }

        public void multiply(float fVar1, float fVar2)
            {
                Console.WriteLine("Product is: " + (fVar1 * fVar2));
            }

        public void divide(float fVar1, float fVar2)
            {
                try
                {
                    Console.WriteLine("Quotient is: " + (fVar1 / fVar2));
                }

                catch
                {
                    Console.WriteLine("Invalid input");
                }
            }
        }

   
       abstract class Interest
        {
            protected double principal, time, rate;
            public abstract double getInterest();
        }

    class simpleInterest : Interest
    {
        public simpleInterest(double principal, double time, double rate) : base () {
            this.principal = principal;
            this.time = time;
            this.rate = rate;
        }

            public override double getInterest()
            {
                return ((principal * time * rate) / 100);
            }
        }

        class compoundInterest : Interest
        {
        public compoundInterest(double principal, double time, double rate) : base() {
            this.principal = principal;
            this.time = time;
            this.rate = rate;
        }
            public override double getInterest()
            {
                return (principal * (Math.Pow((1 + rate), time) - 1));
            }
        }
   
}