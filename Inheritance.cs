﻿using System;

namespace Inheritance
{
    class Polygon
    {
        protected int numberOfSides;

        public virtual double getArea()
        {
            Console.WriteLine("Area is:");
            return 0;
        }
    }

    class Triangle: Polygon
    {
        public float length, height;
        public Triangle(): base ()
        {
            numberOfSides = 3;
        }
        public override double getArea()
        {
            return ((length * height) / 2);
        }
    }
    class Rectangle: Polygon
    {
        public float length, breadth;
        public Rectangle() : base()
        {
            numberOfSides = 4;
        }

        public override double getArea()
        {
            return (length * breadth);
        }
    }
}